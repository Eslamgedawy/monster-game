function getRandomValue(min, max) {
  return Math.floor(Math.random() * (max - min)) + min;
}

var app = Vue.createApp({
  data() {
    return {
      playerHealth: 100,
      monsterHealth: 100,
      roundCounter: 0,
      winner: null,
      logMessages: [],
    };
  },
  methods: {
    // player attacks monster
    attackMonster() {
      // increase round counter
      this.roundCounter++;
      // we want to get attack value
      const attackValue = getRandomValue(5, 12);
      // decrease monsterHealth
      this.monsterHealth -= attackValue;
      console.log(` Player attacks with ${attackValue} value`);
      this.getLogMessages("Player", "attacks with", attackValue);
      // call attackPlayer function here to make the game more relibale
      this.attackPlayer();
    },

    // monster attacks player
    attackPlayer() {
      // we want to get attack value
      const attackValue = getRandomValue(8, 15);
      // decrease PlayerHealth
      this.playerHealth -= attackValue;
      console.log(` Monster replies with ${attackValue} value`);
      this.getLogMessages("Monster", "replies with", attackValue);
    },

    specialAttack() {
      // increase round counter
      this.roundCounter++;
      // we want to get attack value
      const attackValue = getRandomValue(10, 25);
      // decrease monsterHealth
      this.monsterHealth -= attackValue;
      console.log(` Player replies with speical ${attackValue} value`);
      this.getLogMessages("Player", "replies with speical", attackValue);
      // call attackPlayer function here to make the game more relibale
      this.attackPlayer();
    },

    heal() {
      // increase round counter
      this.roundCounter++;
      // we want to get heal value
      const healValue = getRandomValue(8, 20);
      // decrease PlayerHealth
      if (this.playerHealth + healValue > 100) {
        this.playerHealth = 100;
      } else {
        this.playerHealth += healValue;
      }
      console.log(` Player heals with ${healValue} value`);
      this.getLogMessages("Player", "heals with", healValue);
      this.attackPlayer();
    },

    surrender() {
      // when player surrnders the monster won.
      this.winner = "monster";
    },

    newGame() {
      this.playerHealth = 100;
      this.monsterHealth = 100;
      this.roundCounter = 0;
      this.winner = null;
      this.logMessages = [];
    },

    getLogMessages(who, action, value) {
      return this.logMessages.unshift({
        actionBy: who,
        action: action,
        actionValue: value,
      });
    },
  },
  computed: {
    monsterHealthBar() {
      if (this.monsterHealth < 0) {
        return { width: "0%" };
      } else {
        return { width: this.monsterHealth + "%" };
      }
    },
    playerHealthBar() {
      if (this.playerHealth < 0) {
        return { width: "0%" };
      } else {
        return { width: this.playerHealth + "%" };
      }
    },
  },
  watch: {
    playerHealth(val) {
      if (val <= 0 && this.monsterHealth <= 0) {
        this.winner = "draw";
      } else if (val <= 0) {
        this.winner = "monster";
      }
    },
    monsterHealth(val) {
      if (val <= 0 && this.playerHealth <= 0) {
        this.winner = "draw";
      } else if (val <= 0) {
        this.winner = "player";
      }
    },
  },
});

app.mount("#game");
